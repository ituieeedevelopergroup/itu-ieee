package com.ieeedg.ituieee;


import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class HaberIcerik extends Activity {
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hatagonder);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		int position= MainActivity.haber;
		
		WebView habericerik=(WebView) findViewById(R.id.hatagonder);
		
		habericerik.loadUrl(MainActivity.xmlLink.get(position));
	}


}
