package com.ieeedg.ituieee;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.datatype.Duration;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter.LengthFilter;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;



public class MainActivity extends Activity {

	public static int haber = 0;
	public static int etkinliks�ra=0;
	public static int fotograf=0;
	public static int sira=0;
	public static boolean ilk=true;
	public static ArrayList<String> xmlList=new ArrayList<String>();
	public static ArrayList<String> xmlLink=new ArrayList<String>();
	public static ArrayList<String> xmlicerik=new ArrayList<String>();
	public class arkaPlanIsleri extends  AsyncTask<Void, Void, Void> {
		
		
		
		 @Override
		 public void onPostExecute(Void result) {
		  // TODO Auto-generated method stub
			 Thread acilisekrani= new Thread(){
					
					
					public void run(){
						try
						{
							startActivity(new Intent("android.intent.action.RSSOKUYUCU"));
						}
						finally
						{
							finish();
						}
							
							
						}
					
				
					};
					acilisekrani.start();
				
		  
		 }

		 @Override
		 public void onPreExecute() {
		  // TODO Auto-generated method stub
		 }
		
		 @Override
		 public Void doInBackground(Void... params) {
		  // TODO Auto-generated method stub
			 xmlList=getListFromXml("http://www.ituieee.com/feed");
			 xmlLink=getLinkFromXml("http://www.ituieee.com/feed");
			 xmlicerik=getIcerikFromXml("http://www.ituieee.com/feed");
		  return null;
		 }
		 
			 }
		 

	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acilis);
		
		ActionBar actionbar=getActionBar();
		actionbar.hide();
		if(!internetBaglantisiVarMi()){
			Toast.makeText(MainActivity.this, "Uygulamaya girebilmek i�in internet ba�lant�s� gereklidir.",Toast.LENGTH_LONG).show();
		}
		else
		{
		Toast.makeText(MainActivity.this, "Haberler y�kleniyor...",Toast.LENGTH_LONG).show();	
		new arkaPlanIsleri().execute();
		}
		
			
	}
	boolean internetBaglantisiVarMi() {
		 
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService (Context.CONNECTIVITY_SERVICE);
 
        if (conMgr.getActiveNetworkInfo() != null
 
        && conMgr.getActiveNetworkInfo().isAvailable()
 
        && conMgr.getActiveNetworkInfo().isConnected()) {
 
        return true;
 
        } else {
 
        return false;
 
        }
 
    }
	public ArrayList<String> getListFromXml(String strng)  {

		ArrayList<String> list=new ArrayList<String>();
		
		try {

			URL url=new URL(strng);
			DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder=dFactory.newDocumentBuilder();

			Document document=dBuilder.parse(new InputSource(url.openStream()));
			document.getDocumentElement().normalize();
			
			NodeList nodeListCountry=document.getElementsByTagName("item");
			for (int i = 0; i < nodeListCountry.getLength(); i++) {
				Node node=nodeListCountry.item(i);
				Element elementMain=(Element) node;

				NodeList nodeListText=elementMain.getElementsByTagName("title");
				Element elementText=(Element) nodeListText.item(0);
				
				list.add(elementText.getChildNodes().item(0).getNodeValue());
				Log.d("RSS", "List.add");
				
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		return list;
	}


	public ArrayList<String> getLinkFromXml(String strng)  {

		ArrayList<String> list=new ArrayList<String>();
		
		try {

			URL url=new URL(strng);
			DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder=dFactory.newDocumentBuilder();

			Document document=dBuilder.parse(new InputSource(url.openStream()));
			document.getDocumentElement().normalize();
			
			NodeList nodeListCountry=document.getElementsByTagName("item");
			for (int i = 0; i < nodeListCountry.getLength(); i++) {
				Node node=nodeListCountry.item(i);
				Element elementMain=(Element) node;

				NodeList nodeListText=elementMain.getElementsByTagName("guid");
				Element elementText=(Element) nodeListText.item(0);
				
				list.add(elementText.getChildNodes().item(0).getNodeValue());
				Log.d("RSS", "List.add");
				
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		return list;
	}
	public ArrayList<String> getIcerikFromXml(String strng)  {

		ArrayList<String> list=new ArrayList<String>();
		
		try {

			URL url=new URL(strng);
			DocumentBuilderFactory dFactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder=dFactory.newDocumentBuilder();

			Document document=dBuilder.parse(new InputSource(url.openStream()));
			document.getDocumentElement().normalize();
			
			NodeList nodeListCountry=document.getElementsByTagName("item");
			for (int i = 0; i < nodeListCountry.getLength(); i++) {
				Node node=nodeListCountry.item(i);
				Element elementMain=(Element) node;

				NodeList nodeListText=elementMain.getElementsByTagName("description");
				Element elementText=(Element) nodeListText.item(0);
				
				list.add(elementText.getChildNodes().item(0).getNodeValue());
				Log.d("RSS", "List.add");
				
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		return list;
	}
	
}



