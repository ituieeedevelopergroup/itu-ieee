package com.ieeedg.ituieee;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebView;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class RssReader extends FragmentActivity {
	private static String[] etkinlikler;
	private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private static boolean tekgun = false;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mBoardTitles;
    private static ExpandableListAdapter listAdapter;
    private static ExpandableListAdapter etklistAdapter;
    private static List<String> listDataHeader;
    private static List<String> etklistDataHeader;
    private static HashMap<String, List<String>> listDataChild;
    private static HashMap<String, List<String>> etklistDataChild;
    
    

 

@Override

public void onCreate(Bundle savedInstanceState) {
	
	 super.onCreate(savedInstanceState);
	 setContentView(R.layout.activity_main);
	
	 mDrawerTitle = getTitle();
     mBoardTitles = getResources().getStringArray(R.array.boards_array);
     mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
     mDrawerList = (ListView) findViewById(R.id.left_drawer);
     //mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
     mDrawerList.setAdapter(new ArrayAdapter<String>(this,
             R.layout.drawer_list_item, mBoardTitles));
     etkinlikler=getResources().getStringArray(R.array.etkinlik_array);
     
	 
	 mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
	
	 getActionBar().setDisplayHomeAsUpEnabled(true);
     getActionBar().setHomeButtonEnabled(true);
     mDrawerToggle = new ActionBarDrawerToggle(
             this,                  
             mDrawerLayout,         
             R.drawable.ic_drawer, 
             R.string.drawer_open, 
             R.string.drawer_close  
             ){
    	 public void onDrawerClosed(View view) {
         getActionBar().setTitle(mTitle);
         invalidateOptionsMenu(); 
     }

     public void onDrawerOpened(View drawerView) {
         getActionBar().setTitle(mDrawerTitle);
         invalidateOptionsMenu(); 
     }
 };
 mDrawerLayout.setDrawerListener(mDrawerToggle);

 if (savedInstanceState == null) {
     selectItem(MainActivity.sira);
 	}
 
	 
}




@Override
public boolean onCreateOptionsMenu(Menu menu) {
	
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
}


@Override
public boolean onPrepareOptionsMenu(Menu menu) {

boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
// menu.findItem(R.id.action_search).setVisible(!drawerOpen);
return super.onPrepareOptionsMenu(menu);
}

public boolean onOptionsItemSelected(MenuItem item) {
    
   if (mDrawerToggle.onOptionsItemSelected(item)) {
       return true;
   }
  
   switch(item.getItemId()) {
   case R.id.action_settings:
       
       //Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
       //intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
       // catch event that there's no activity to handle intent
       //if (intent.resolveActivity(getPackageManager()) != null) {
          startActivity(new Intent("android.intent.action.HATAGONDER"));
      // } else {
          // Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
      // }
       //return true;
   default:
       return super.onOptionsItemSelected(item);
   }
}


private class DrawerItemClickListener implements ListView.OnItemClickListener {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectItem(position);
    }
}

private void selectItem(int position) {
   
    	if(position==10){
	Fragment fragmenta = new IletisimFragment();
	Bundle args = new Bundle();
	args.putInt(ListFragment.ARG_BOARD_NUMBER, position);
	fragmenta.setArguments(args);
	FragmentManager fragmentManager = getFragmentManager();
    fragmentManager.beginTransaction().replace(R.id.list_frame, fragmenta).commit();
    	}
    	else
    	{
    	Fragment fragmentb = new ListFragment();
    	Bundle args = new Bundle();
    	args.putInt(ListFragment.ARG_BOARD_NUMBER, position);
    	fragmentb.setArguments(args);
    	FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.list_frame, fragmentb).commit();
    	}
    
    mDrawerList.setItemChecked(position, true);
    setTitle(mBoardTitles[position]);
    mDrawerLayout.closeDrawer(mDrawerList);
}

@Override
public void setTitle(CharSequence title) {
    mTitle = title;
    getActionBar().setTitle(mTitle);
}

protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    
    mDrawerToggle.syncState();
}

@Override
public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
   
    mDrawerToggle.onConfigurationChanged(newConfig);
}



public static class ListFragment extends Fragment {
    public static final String ARG_BOARD_NUMBER = "board_number";
    
    public ListFragment() {
       
    }
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            Bundle savedInstanceState) {
    	
    	final int i = getArguments().getInt(ARG_BOARD_NUMBER);
    	MainActivity.sira=i;
        View rootView = inflater.inflate(R.layout.fragment_board, container, false);
   	 
        String board = getResources().getStringArray(R.array.boards_array)[MainActivity.sira];

        int imageId = getResources().getIdentifier(board.toLowerCase(Locale.getDefault()).replace(" ", "_").replace("�", "i"),
                        "drawable", getActivity().getPackageName());
        ((ImageView) rootView.findViewById(R.id.image)).setImageResource(imageId);
        getActivity().setTitle(board);
        
        TextView iceriktw=(TextView)rootView.findViewById(R.id.iceriktext);
        Button katil=(Button) rootView.findViewById(R.id.katil);
        Activity activity=getActivity();
        
        String[] icerikler =activity.getResources().getStringArray(R.array.icerik_array);
        if(MainActivity.sira!=10 && MainActivity.sira!=9)
        iceriktw.setText(icerikler[i]);
    	
        
        if(MainActivity.sira!=1){
        	katil.setAlpha(1.0f);
        	//listView1.setAlpha(0.0f);
        }
        
        katil.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(MainActivity.sira==2){
					
					Uri link = Uri.parse("https://www.facebook.com/groups/itu.ieee.cs/");
					final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
				else if(MainActivity.sira==3){
					
					Uri link = Uri.parse("https://www.facebook.com/groups/ituieeecomsoc/");
					final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
				else if(MainActivity.sira==4){
					
					Uri link = Uri.parse("https://www.facebook.com/groups/ituieeekariyer2014/");
					final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
				else if(MainActivity.sira==5){
					
					Uri link = Uri.parse("https://www.facebook.com/groups/lcistanbul1213/");
					final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}				
				else if(MainActivity.sira==6){
					
					Uri link = Uri.parse("https://www.facebook.com/groups/ITUWIE/");
					final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
				else if(MainActivity.sira==7){
					
					Uri link = Uri.parse("https://www.facebook.com/groups/450130631710560/");
					final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
				else if(MainActivity.sira==8){
					
					Uri link = Uri.parse("https://www.facebook.com/groups/basyaykom/");
					final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
			}
		});
        View etkinlikview = inflater.inflate(R.layout.expandablelist, container, false);
        ExpandableListView expetkListView = (ExpandableListView) etkinlikview.findViewById(R.id.lvExp);
        etkprepareListData();
        
        etklistAdapter = new ExpandableListAdapter(getActivity(), etklistDataHeader, etklistDataChild);
 
        // setting list adapter
        expetkListView.setAdapter(etklistAdapter);
        
        expetkListView.setOnChildClickListener(new OnChildClickListener(){

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				if(childPosition==1 && groupPosition==0)
				{
					tekgun=false;
					takvimeEkle("�VOFest 2014", "�T� Ayaza�a Kamp�s�", 2014, 3, 28);
				}
				if(childPosition==0 && groupPosition==0)
				{
					MainActivity.etkinliks�ra=groupPosition;
					startActivity(new Intent("android.intent.action.ETKINLIKICERIK"));
				}
				if(childPosition==1 && groupPosition==1)
				{
					tekgun=true;
					takvimeEkle("Yaz�l�m Maratonu 2014", "�T� Elektrik Elektronik Fak�ltesi", 2014, 3, 26);
				}
				if(childPosition==0 && groupPosition==1)
				{
					MainActivity.etkinliks�ra=groupPosition;
					startActivity(new Intent("android.intent.action.ETKINLIKICERIK"));
				}
				if(childPosition==1 && groupPosition==2)
				{
					tekgun=false;
					takvimeEkle("D�nden Yar�na D�nya", "�T� S�leyman Demirel K�lt�r Merkezi", 2014, 3, 16);
				}
				if(childPosition==0 && groupPosition==2)
				{
					MainActivity.etkinliks�ra=groupPosition;
					startActivity(new Intent("android.intent.action.ETKINLIKICERIK"));
				}
				
				
				return false;
			}
        	
        });
        
        View listeView = inflater.inflate(R.layout.list_board, container, false);
        ListView listView1 = (ListView) listeView.findViewById(R.id.list);
        listView1.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.etkinlik_list_item, MainActivity.xmlList));
        View listeViewe = inflater.inflate(R.layout.expandablelist, container, false);
        
        ExpandableListView expListView = (ExpandableListView) listeViewe.findViewById(R.id.lvExp);
        
        if(MainActivity.ilk==true)
        prepareListData();
        
        
        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);
        expListView.setOnChildClickListener(new OnChildClickListener() {
			
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				
				MainActivity.haber= groupPosition;
				MainActivity.ilk=false;
				Uri link = Uri.parse(MainActivity.xmlLink.get(groupPosition));
       		  
				
         		final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
         		
         		startActivity(openBrowser);
				
				return false;
			}
		}); {
			
		
        //MyCustomAdapter adapter = new MyCustomAdapter(getActivity(), R.layout.list, xmlList);
  	  	//listView1.setAdapter(adapter);
		  Log.d("Haberler", "Adapter1");
        
		View etkinliklerView=inflater.inflate(R.layout.list_board, container,false);
		ListView etkinliklerlist=(ListView) etkinliklerView.findViewById(R.id.list);
		
		etkinliklerlist.setAdapter(new ArrayAdapter<String>(getActivity(),
	     R.layout.etkinlik_list_item, etkinlikler));
		 Log.d("Etkinlikler", "Liste A��ld�");
		
				listView1.setOnItemClickListener(new OnItemClickListener() {
		        
		         	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
		         		
		         		MainActivity.haber = position;
		         		
						/*Uri link = Uri.parse(MainActivity.xmlLink.get(position));
		         		  
						
		         		final Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
		         		 */ 
		         		startActivity(new Intent("android.intent.action.HABERICERIK"));
		         		
						
		         	}

		         	});
				
				if(MainActivity.sira==0)
		        return listeViewe;
		        else if(MainActivity.sira==9)
		        return etkinlikview;
		        else
		        return rootView;
    }}
    
   
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
 
        // Adding child data
        listDataHeader.add(MainActivity.xmlList.get(MainActivity.haber));
        listDataHeader.add(MainActivity.xmlList.get(MainActivity.haber+1));
        listDataHeader.add(MainActivity.xmlList.get(MainActivity.haber+2));
        listDataHeader.add(MainActivity.xmlList.get(MainActivity.haber+3));
        listDataHeader.add(MainActivity.xmlList.get(MainActivity.haber+4));
        int kod=MainActivity.xmlicerik.get(MainActivity.haber).indexOf("<a");
        int koda=MainActivity.xmlicerik.get(MainActivity.haber+1).indexOf("<a");
        int kodb=MainActivity.xmlicerik.get(MainActivity.haber+2).indexOf("<a");
        int kodc=MainActivity.xmlicerik.get(MainActivity.haber+3).indexOf("<a");
        int kodd=MainActivity.xmlicerik.get(MainActivity.haber+4).indexOf("<a");
 
        // Adding child data
        List<String> birinci = new ArrayList<String>();
        if(kod!=-1)
        birinci.add(MainActivity.xmlicerik.get(MainActivity.haber).substring(0,kod).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        else
        birinci.add(MainActivity.xmlicerik.get(MainActivity.haber).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        
        List<String> ikinci = new ArrayList<String>();
        if(koda!=-1)
        ikinci.add(MainActivity.xmlicerik.get(MainActivity.haber+1).substring(0,koda).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        else
        ikinci.add(MainActivity.xmlicerik.get(MainActivity.haber+1).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
 
        List<String> ucuncu = new ArrayList<String>();
        if(kodb!=-1)
        ucuncu.add(MainActivity.xmlicerik.get(MainActivity.haber+2).substring(0,kodb).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        else
        ucuncu.add(MainActivity.xmlicerik.get(MainActivity.haber+2).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        
        List<String> dorduncu = new ArrayList<String>();
        if(kodc!=-1)
        dorduncu.add(MainActivity.xmlicerik.get(MainActivity.haber+3).substring(0,kodc).replace("&#8211","-").replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        else
        dorduncu.add(MainActivity.xmlicerik.get(MainActivity.haber+3).replace("&#8211","-").replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
 
        List<String> besinci = new ArrayList<String>();
        if(kodd!=-1)
        besinci.add(MainActivity.xmlicerik.get(MainActivity.haber+4).substring(0,kodd).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        else
        besinci.add(MainActivity.xmlicerik.get(MainActivity.haber+4).replace("&#8217;", "'").replace("&#8220;&#8230;", "").replace("&#8242;", "'"));
        
        listDataChild.put(listDataHeader.get(0), birinci);
        listDataChild.put(listDataHeader.get(1), ikinci);
        listDataChild.put(listDataHeader.get(2), ucuncu);
        listDataChild.put(listDataHeader.get(3), dorduncu); 
        listDataChild.put(listDataHeader.get(4), besinci);
        
    }
    private void etkprepareListData() {
        etklistDataHeader = new ArrayList<String>();
        etklistDataChild = new HashMap<String, List<String>>();
 
        
        etklistDataHeader.add("�VOFest 2014");
        etklistDataHeader.add("Yaz�l�m Maratonu 2014");
        etklistDataHeader.add("D�nden Yar�na D�nya");
        //etklistDataHeader.add("Communications Week");
        //etklistDataHeader.add("�T� Kariyer Zirvesi");
        
 
        List<String> bir = new ArrayList<String>();
        bir.add("Bilgi Al");
        bir.add("Takvimine Ekle");
        
        
        List<String> iki = new ArrayList<String>();
        iki.add("Bilgi Al");
        iki.add("Takvimine Ekle");
        
 
        List<String> uc = new ArrayList<String>();
        uc.add("Bilgi Al");
        uc.add("Takvimine Ekle");
        
        //List<String> dort = new ArrayList<String>();
        //dort.add("Bilgi Al");
        //dort.add("Takvimine Ekle");
 
        //List<String> bes = new ArrayList<String>();
        //bes.add("Bilgi Al");
        //bes.add("Takvimine Ekle");
 
        etklistDataChild.put(etklistDataHeader.get(0), bir);
        etklistDataChild.put(etklistDataHeader.get(1), iki);
        etklistDataChild.put(etklistDataHeader.get(2), uc);
        //etklistDataChild.put(etklistDataHeader.get(3), dort); 
        //etklistDataChild.put(etklistDataHeader.get(4), bes);
        
    }
    public void takvimeEkle(String etkinlikadi, String mekan,int yil,int ay,int gun){
    	
    	Intent intent = new Intent(Intent.ACTION_INSERT);
		intent.setType("vnd.android.cursor.item/event");
		intent.putExtra(Events.TITLE, etkinlikadi);
		intent.putExtra(Events.EVENT_LOCATION, mekan);
		
		GregorianCalendar calDate = new GregorianCalendar(yil, ay, gun);
		intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
		  calDate.getTimeInMillis());
		intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
		  calDate.getTimeInMillis());

		
		intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);

		
		if(tekgun!=true)
		intent.putExtra(Events.RRULE, "FREQ=DAILY;COUNT=2");
		intent.putExtra(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
		intent.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
		
		startActivity(intent);
    	
    }
    
    
}
public static class IletisimFragment extends Fragment {
    public static final String ARG_BOARD_NUMBER = "board_number";
    
    public IletisimFragment() {
       
    }
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            Bundle savedInstanceState) {
    		final String url= "https://maps.google.com/maps?q=%C4%B0t%C3%BC+Elektrik+Elektronik+Fak%C3%BCltesi,+Re%C5%9Fitpa%C5%9Fa+Mh.,+%C4%B0stanbul%2F%C4%B0stanbul,+T%C3%BCrkiye&hl=tr&ie=UTF8&ll=41.105104,29.025085&spn=0.004923,0.010568&sll=37.0625,-95.677068&sspn=42.224734,86.572266&oq=it%C3%BC+elektrik+el&t=h&hq=%C4%B0t%C3%BC+Elektrik+Elektronik+Fak%C3%BCltesi,&hnear=Re%C5%9Fitpa%C5%9Fa+Mh.,+%C4%B0stanbul%2FSar%C4%B1yer%2F%C4%B0stanbul,+T%C3%BCrkiye&z=17";
    		View iletisim = inflater.inflate(R.layout.iletisim_fragment, container, false);
    		
    		ImageButton harita=(ImageButton) iletisim.findViewById(R.id.harita);
    		ImageButton smf=(ImageButton) iletisim.findViewById(R.id.facebook);
    		ImageButton smt=(ImageButton) iletisim.findViewById(R.id.twitter);
    		ImageButton smg=(ImageButton) iletisim.findViewById(R.id.gplus);
    		ImageButton smi=(ImageButton) iletisim.findViewById(R.id.linkedin);
    		ImageButton smins=(ImageButton) iletisim.findViewById(R.id.instagram);
    		
    		smf.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Uri link = Uri.parse("https://www.facebook.com/ituieee");
					Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
			});
    		smt.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Uri link = Uri.parse("https://twitter.com/itu_ieee");
					Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
			});
    		smg.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Uri link = Uri.parse("https://plus.google.com/+Ituieeeogrencikolu/posts");
					Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
			});
    		smi.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Uri link = Uri.parse("http://www.linkedin.com/company/2103261?trk=tyah&trkInfo=tas%3Ait%FC%20ieee%2Cidx%3A1-1-1");
					Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
			});
    		smins.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Uri link = Uri.parse("http://instagram.com/ituieee");
					Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
				}
			});
    		harita.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					
					Uri link = Uri.parse(url);
					Intent openBrowser = new Intent(Intent.ACTION_VIEW,link);
			 		  
			 		startActivity(openBrowser);
					
				}
			});
    		
    		
				return iletisim;
    }
}
}


