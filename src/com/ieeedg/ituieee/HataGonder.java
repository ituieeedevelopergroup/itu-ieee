package com.ieeedg.ituieee;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class HataGonder extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.hatagonder);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		WebView egitim=(WebView) findViewById(R.id.hatagonder);
		
		egitim.loadUrl("https://docs.google.com/forms/d/1rarGYIxtm3Xrx2NWHchYTYPbHM6O6IYuYMLfl901fwM/viewform");
	}
	
	

}
