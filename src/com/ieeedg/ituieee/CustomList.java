package com.ieeedg.ituieee;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomList extends ArrayAdapter<String>{
	
private final Activity context;
private final String[] ivooturumlar;
public CustomList(Activity context,
String[] ivooturumlar) {
super(context, R.layout.etkinliklerlistesiicerik, ivooturumlar);
this.context = context;
this.ivooturumlar = ivooturumlar;


}
@Override
public View getView(int position, View view, ViewGroup parent) {
LayoutInflater inflater = context.getLayoutInflater();
View rowView= inflater.inflate(R.layout.etkinliklerlistesiicerik, null, true);
TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

txtTitle.setText(ivooturumlar[position]);

return rowView;
}
}
