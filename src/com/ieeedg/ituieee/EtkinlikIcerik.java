package com.ieeedg.ituieee;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class EtkinlikIcerik extends Activity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.etkinlikicerik);
		
		
		TextView etkinlikbaslik=(TextView) findViewById(R.id.etkinlikbaslik);
		TextView etkinlikicerik=(TextView) findViewById(R.id.etkinlikicerik);
		TextView oturumlar=(TextView) findViewById(R.id.konusmacilar);
		final ListView oturumlarliste=(ListView) findViewById(R.id.oturumliste);
		TextView detaylar=(TextView) findViewById(R.id.detaylar);
		
		if(MainActivity.etkinliks�ra==0)
			
		{
			String[] ivooturumlar={"Video Oyun Semineri ��geni","Panel: Oyunun Tuzu Biberi","E�itim: Unity 3D"};
			etkinlikbaslik.setText("�T� Video Oyun Festivali 2014");
			etkinlikicerik.setText("28-29 Nisan 2014'te �T� Ayaza�a Kamp�s�'nde!");
			//oturumlarliste.setAdapter(new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,ivooturumlar));
			CustomList adapter = new
					CustomList(EtkinlikIcerik.this, ivooturumlar);
			oturumlarliste.setAdapter(adapter);
		}
		if(MainActivity.etkinliks�ra==1)
		{
			etkinlikbaslik.setText("Yaz�l�m Maratonu 2014");
			etkinlikicerik.setText("�n Eleme ��in Son Ba�vuru Tarihi:\n" +
					"13 Nisan 2014  Saat 23:59\n\n" +
					"�n Eleme Sorular�n�n Site �zerinden Cevaplanmas�:\n" +
					"14 Nisan 2014   10:00 //  16 Nisan 2014 10:00\n\n" +
					"De�erlendirmelerin Yap�l�p Finalist Tak�mlar�n Duyurulmas�:\n" +
					"20 Nisan 2014\n\n" +
					"Final Etab�:\n" +
					"26 Nisan 2014");
			oturumlar.setAlpha(0.0f);
			detaylar.setAlpha(0.0f);
		}
		if(MainActivity.etkinliks�ra==2)
		{
			String[] ivooturumlar={"A��k Olmak Gerekirse..","Bosch Senin ��in Burada","Konu�abilir Misin?","M�hendislikte Kad�n","Fark�n� Ortaya Koy","Biri Beni Ba�tan Yarats�n","Fark Yaratmak: Teknoloji ve Kad�n","Ba�aramamak Nereden Geldi?","H�zl� ve H�rsl�","Ford Otosan'a Bir Yolculuk","Panel: Kad�n ve Liderlik Sanat�","Sosyal Konuk","Foto�raf Yar��mas� �d�l T�reni"};
			etkinlikbaslik.setText("D�nden Yar�na D�nya");
			etkinlikicerik.setText("16-17 Nisan 2014'te �T� S�leyman Demirel K�lt�r Merkezi'nde!");
			//oturumlarliste.setAdapter(new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,dydoturumlar));
			CustomList adapter = new
					CustomList(EtkinlikIcerik.this, ivooturumlar);
			oturumlarliste.setAdapter(adapter);
		}
		oturumlarliste.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				AlertDialog.Builder icerik=new AlertDialog.Builder(EtkinlikIcerik.this);
				
				if(position==0 && MainActivity.etkinliks�ra==0)
				{
					icerik.setTitle("Video Oyun Semineri ��geni");
					icerik.setMessage("Konu�mac�lar: �zg�r Soner, Yasin Demirden ve Fevzi Altuncu\n\n" +
							"28 Nisan 2014\n10.00-13.00");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==1 && MainActivity.etkinliks�ra==0)
				{
					icerik.setTitle("Panel: Oyunun Tuzu Biberi");
					icerik.setMessage("Konu�mac�lar: �zg�r Soner, Sarp K�rkc�, Burak Tezate�er, Ba�ar Simit�i ve Engin Ya��z Hatay\n\n29 Nisan 2014\n10.30-12.30");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
					}
				else if(position==2 && MainActivity.etkinliks�ra==0)
				{
					icerik.setTitle("E�itim: Unity 3D");
					icerik.setMessage("Konu�mac�: Engin Y�ld�z\n\n28-29 Nisan 2014\n13.30-16.30");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==0 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("A��k Olmak Gerekirse..");
					icerik.setMessage("Konu�mac�: Simge F�st�ko�lu \n\n16 Nisan 2014\n12.15-13.15");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==1 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Bosch Senin ��in Burada");
					icerik.setMessage("16 Nisan 2014\n13.45-14.10");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==2 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Konu�abilir Misin?");
					icerik.setMessage("Konu�mac�: Funda Bilgili \n\n16 Nisan 2014\n14.20-15.20");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==3 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("M�hendislikte Kad�n");
					icerik.setMessage("Konu�mac�: Berna Dengiz \n\n16 Nisan 2014\n15.30-16.30");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==4 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Fark�n� Ortaya Koy");
					icerik.setMessage("Konu�mac�: Arda Os \n\n16 Nisan 2014\n16.40-17.40");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==5 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Biri Beni Ba�tan Yarats�n");
					icerik.setMessage("Konu�mac�: Banu G�k�il \n\n16 Nisan 2014\n17.50-18.50");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==6 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Fark Yaratmak: Teknoloji ve Kad�n");
					icerik.setMessage("17 Nisan 2014\n12.00-12.25");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==7 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Ba�aramamak Nereden Geldi?");
					icerik.setMessage("Konu�mac�: Aret Vartanyan \n\n17 Nisan 2014\n13.00-14.00");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==8 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("H�zl� ve H�rsl�");
					icerik.setMessage("Konu�mac�: Burcu Burkut Erenkul \n\n17 Nisan 2014\n14.10-15.10");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==9 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Ford Otosan'a Bir Yolculuk");
					icerik.setMessage("17 Nisan 2014\n15.20-15.45");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==10 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Panel: Kad�n ve Liderlik Sanat�");
					icerik.setMessage("17 Nisan 2014\n15:55-16:55");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==11 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Sosyal Konuk");
					icerik.setMessage("Kerem Bursin\n\n17 Nisan 2014\n17:05-18:05");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
				});
				}
				else if(position==12 && MainActivity.etkinliks�ra==2)
				{
					icerik.setTitle("Foto�raf Yar��mas� �d�l T�reni");
					icerik.setMessage("17 Nisan 2014\n18:05-18:35");
					icerik.setNeutralButton("Kapat",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
						
						
				});
				}
				
				AlertDialog alertDialog = icerik.create();
				
				alertDialog.show();
			}
		});
	}
	
}

